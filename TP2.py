
# 1)Calcular el perimetro de un rectángulo dado base y altura


def PerRectangulo(x, y):
    perimetro = x * 2 + y * 2
    print("el perímetro del rectángulo es: ", perimetro)
PerRectangulo()

# 2)Calcular el área de un rectángulo dado base y altura


def AreaRectangulo(x, y):
    x = int(input("ingrese la altura del rectángulo: "))
    y = int(input("ingrese la base del rectángulo: "))
    area = x * y
    print("el área del rectángulo es: ", area)
AreaRectangulo()

# 3)calcular el área de un rectángulo dadas las coordenadas x1, x2, x3 y x4


def AreaRectanguloX():
    c1 = []
    c2 = []
    c3 = []
    c4 = []
    for i in range(2):
        a = int(input("Ingrese un valor en x, luego uno en y: "))
        c1.append(a)
        b = int(input("Ingrese un valor en x, luego uno en y: "))
        c2.append(b)
        c = int(input("Ingrese un valor en x, luego uno en y: "))
        c3.append(c)
        d = int(input("Ingrese un valor en x, luego uno en y: "))
        c4.append(d)
    base = float(c2[0]) - float(c1[0])
    altura = float(c4[1]) - float(c1[1])
    area = base * altura
    perimetro = base * 2 + altura * 2
    print("El perímetro es: ", perimetro)
    print("El área es: ", area)
AreaRectanguloX()

# 4)calcular área y perimetro de un circulo dado su radio


def RadioCirculo(x):
    x = int(input("ingrese el radio del circulo: "))
    y = 2 * 3.14159265359 * x
    x = 3.14159265359 * (x**2)
    print("el area del circulo es: ", x)
    print("el perimetro del circulo es: ", y)
RadioCirculo()

# 5)calcular el volumen de una esfera dado su radio


def VolEsfera(x):
    x = int(input("ingrese el radio de la esfera: "))
    x = 4/3 * (3.14159265359 * (x**3))
    print("el volumen de la esfera es: ", x)
VolEsfera()
